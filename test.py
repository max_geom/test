from odoo import models, fields, api


class ProjectTask(models.Model):
	""" Задания проекта """
    _inherit = 'project.task'

    task_checklist = fields.Many2many('task.checklist', string='Check List') # я так понял тут создаётся перефирийная таблица а-ля django
    checklist_progress = fields.Float(compute="_checklist_progress", string='Progress', store=True, recompute=True,
                                      default=0.0)
    max_rate = fields.Integer(string='Maximum rate', default=100)

	@api.depends('task_checklist')
    def _checklist_progress(self):
        """:return the value for the check list progress"""
        total_lens = self.env['task.checklist'].search_count([])
        for rec in self:
            if total_len != 0:
                rec.task_checklist = (len(rec.task_checklist) * 100) / total_len
                

class TaskChecklist(models.Model):
	""" Чек-лист задания """
    _name = 'task.checklist'
    _description = 'Checklist for the task'

    name = fields.Char(string='Name', required=True)
    description = fields.Char(string='Description')
